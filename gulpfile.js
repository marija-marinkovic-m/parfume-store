/* global gulp, config */
'use strict';

global.config = require('./tasks/config');
global.gulp = require('gulp');
global.del = require('del');
global.plumber = require('gulp-plumber');
global.notify = require('gulp-notify');

require('./tasks/development/html');
require('./tasks/development/sass');
require('./tasks/development/icons');
require('./tasks/development/images');
require('./tasks/development/fonts');
require('./tasks/development/favicons');
require('./tasks/development/javascript');
require('./tasks/development/webserver');
require('./tasks/development/deploy');
require('./tasks/development/dist');
require('./tasks/development/videos');
require('./tasks/development/sounds');

gulp.task('watch', ['webserver'], function() {
  gulp.watch(config.srcPath + config.markupDir + '**/*.html', ['nunjucks']);
  gulp.watch(config.srcPath + config.sassDir + '**/*.scss', ['sass']);
  gulp.watch(config.srcPath + config.iconsDir + '**/*.svg', ['icons']);
  gulp.watch(config.srcPath + config.imgDir + '**/*', ['images']);
  gulp.watch(
    config.srcPath + config.fontsDir + '**/*.{ttf,woff,eof,eot,otf,svg}',
    ['fonts']
  );
  gulp.watch(config.srcPath + config.faviconDir + 'favicon.png', ['favicons']);
  gulp.watch(config.srcPath + config.jsDir + '**/*.js', ['js']);
  gulp.watch(config.srcPath + config.videosDir + '*', ['videos']);
  gulp.watch(config.srcPath + config.soundsDir + '*', ['sounds']);
});

gulp.task('lint', ['js-lint', 'scss-lint', 'html-lint']);

gulp.task('build', ['lint', 'default', 'dist']);

gulp.task(
  'default',
  [
    'nunjucks',
    'favicons',
    'sass',
    'icons',
    'fonts',
    'images',
    'js',
    'videos',
    'sounds'
  ]
);
