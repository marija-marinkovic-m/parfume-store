var mat4 = require('gl-mat4');
var vec4 = require('gl-vec4');
var icosphere = require('icosphere');
var ctx = require('./create-canvas-context');
var transformPoints = require('./drawing').transformPoints;
var prepareModel = require('./drawing').prepareModel;
var drawPoints = require('./drawing').drawPoints;
var drawPolygons = require('./drawing').drawPolygons;

var ROTATE_SPEED = 0.00004;
var RANDOMIZE_SPEED = 0.05;
var RANDOMIZE_AMPLITUDE = 0.0006;

module.exports = (function() {
  var center = [];
  var width = 0;
  var height = 0;
  var sphereModel = icosphere(2);
  var timeStarted = Date.now();
  var rotateMatrix = mat4.create();
  var pvMatrix = mat4.create();
  prepareModel(sphereModel, 120);

  function getPVMatrix() {
    // Prepare view matrix
    var viewMatrix = mat4.lookAt([],
      // Camera position
      [0, 40, 390],
      // Target position
      [0, 0, 0],
      // Camera orientation
      [0, 1, 0]
    );

    // Prepare projection matrix
    var projectionMatrix = mat4.perspective([],
      // Camera angle
      Math.PI / 4,
      // Aspect ratio
      1,
      // Near plane
      0,
      // Far plane
      10
    );

    return mat4.multiply([], projectionMatrix, viewMatrix);
  }

  function update(dt) {
    var elapsed = Date.now() - timeStarted;
    var length = sphereModel.positions.length;
    var transformMatrix = mat4.create();
    var scaleMatrix = mat4.scale([], mat4.create(), [2.5, 2.5, 2.5]);
    var transformMatrixBig = mat4.create();

    // Pseudo-randomize individual points
    sphereModel.positions.forEach(function(position, idx) {
      var phase = 360 / length * idx;
      var angle = RANDOMIZE_SPEED * elapsed % 360;

      vec4.scale(position, position,
        1 + RANDOMIZE_AMPLITUDE * Math.sin((phase + angle) * Math.PI / 180)
      );
    });

    // Update rotate matrix
    mat4.rotateY(rotateMatrix, rotateMatrix, ROTATE_SPEED * dt);
    // Multiply with projection matrix
    mat4.multiply(transformMatrix, pvMatrix, rotateMatrix);

    // Prepare matrix for bigger sphere
    mat4.multiply(transformMatrixBig, transformMatrix, scaleMatrix);

    // Apply transformations
    var positions = transformPoints(sphereModel.positions, transformMatrix);
    var positionsBig =
      transformPoints(sphereModel.positions, transformMatrixBig);

    sphereModel.positions2d = positions;
    sphereModel.positions2dBig = positionsBig;
  }

  function drawSphere() {
    drawPoints(ctx, sphereModel.positions2d, center);
    drawPoints(ctx, sphereModel.positions2dBig, center);
    drawPolygons(ctx, sphereModel.cells, sphereModel.positions2d, center);
    drawPolygons(ctx, sphereModel.cells, sphereModel.positions2dBig, center);
  }

  function draw() {
    // Clear canvas
    ctx.clearRect(0, 0, width, height);
    drawSphere();
  }

  function onTick(dt) {
    width = ctx.canvas.width;
    height = ctx.canvas.height;

    // Get new canvas center
    center = [width / 2, height / 2];

    update(dt);
    draw();
  }

  pvMatrix = getPVMatrix();

  return {
    onTick: onTick
  };
})();
