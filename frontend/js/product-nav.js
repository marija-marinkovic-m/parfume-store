var delegate = require('dom-delegate');
var $ = require('./helpers').$;
var $$ = require('./helpers').$$;
var animateIn = require('./cssanims').animateIn;
var animateOut = require('./cssanims').animateOut;
var classNames = require('./class-names');

var eventDelegate = delegate(document.body);
var menuOpen = false;

var autoCloseTimeout = null;

function openMenu() {
  $('.js-product-nav-btn').classList.add('is-active');
  var navList = $('.js-product-nav-list');
  var navElements = $$('.js-product-nav-link', navList);
  animateIn(navList);
  navElements.forEach(animateIn);
}

function closeMenu() {
  $('.js-product-nav-btn').classList.remove('is-active');
  var navList = $('.js-product-nav-list');
  var navElements = $$('.js-product-nav-link', navList);
  animateOut($('.js-product-nav-list'));
  navElements.forEach(function(el) {
    setTimeout(function() {
      el.classList.add(classNames.transparent);
    });
  });
}

// Attach event handlers
eventDelegate.on('click', '.js-product-nav-btn', function() {
  menuOpen = !menuOpen;
  menuOpen ? openMenu() : closeMenu();
});

eventDelegate.on('click', '.js-product-nav-link', function() {
  var active = $('.js-product-nav-link.is-active');
  if (active) active.classList.remove('is-active');
  this.classList.add('is-active');
});

eventDelegate.on('mouseover', '.js-product-nav-list', function() {
  clearTimeout(autoCloseTimeout);
});

eventDelegate.on('mouseout', '.js-product-nav-list', function() {
  autoCloseTimeout = setTimeout(function() {
    if (menuOpen) {
      menuOpen = false;
      closeMenu();
    }
  }, 3000);
});
