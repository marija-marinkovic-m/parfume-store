var delegate = require('dom-delegate');
var Barba = require('barba.js');
var debounce = require('lodash.debounce');

var NEXT_PAGE = 'products.html';
var eventDelegate = delegate(document.body);

var lastY;

function goToNextPage() {
  var current = Barba.Pjax.getCurrentUrl();

  // Check if current page is not products page,
  // to prevent hard refresh (Barba.js bug)
  if (current.indexOf(NEXT_PAGE) === -1) {
    Barba.Pjax.goTo(NEXT_PAGE);
  }
}

function onMouseWheel(e) {
  var delta = e.deltaY;
  if (delta > 0) goToNextPage();
}

function onTouchMove(e) {
  var currentY = e.touches[0].clientY;
  // If moved down
  if (currentY > lastY) {
    goToNextPage();
  } else {
    lastY = currentY;
  }
}

// Attach event handler
eventDelegate.on('wheel', '.js-intro', debounce(onMouseWheel, 300));
eventDelegate.on('touchmove', '.js-intro', onTouchMove);

