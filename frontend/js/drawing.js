var vec4 = require('gl-vec4');

var FILL_STYLE = '#000';

module.exports = (function() {
  function transformPoints(points, matrix) {
    var result = points.map(function(point) {
      return vec4.transformMat4([], point, matrix);
    });

    return result;
  }

  function prepareModel(model, scale) {
    // Convert vec3 model positions to vec4 and scale model
    var positions = model.positions.map(function(position) {
      return vec4.fromValues(
        position[0] * scale,
        position[1] * scale,
        position[2] * scale,
        1
      );
    });

    model.positions = positions;
  }

  function getDepth(z) {
    var depth = 100 / z;
    if (depth < 0) depth = 0;
    return depth;
  }

  function getAlpha(i) { return 0.1 + 0.07 * i; }

  function getLineWidth(i) { return 0.05 + 0.3 * i; }

  function getRadius(i) { return 0.5 + 1.5 * i; }

  function drawPoint(ctx, point, center) {
    var z = point[3];
    // Don't draw points outside view;
    if (z < 0) return;

    var x = point[0] + center[0];
    var y = point[1] + center[1];
    var depth = getDepth(z);
    var radius = getRadius(depth);
    ctx.beginPath();
    ctx.globalAlpha = getAlpha(depth);
    ctx.moveTo(x, y);
    ctx.arc(x, y, radius, 0, 2 * Math.PI);
    ctx.fill();
  }

  function drawPoints(ctx, points, center) {
    points.forEach(function(point) {
      drawPoint(ctx, point, center);
    });
  }

  function drawPolygon(ctx, polygon, points, center) {
    var z = 0;
    var depth = 0;

    ctx.beginPath();
    ctx.lineJoin = 'bevel';
    ctx.fillStyle = FILL_STYLE;
    ctx.strokeStyle = FILL_STYLE;

    // Move to first vertex
    var first = points[polygon[0]];
    ctx.moveTo(first[0] + center[0], first[1] + center[1]);

    // Draw vertices
    for (var i = 1; i < polygon.length; i++) {
      var point = points[polygon[i]];
      z = point[3];
      depth = getDepth(z);
      ctx.lineWidth = getAlpha(depth);
      ctx.globalAlpha = getAlpha(depth);
      ctx.lineTo(point[0] + center[0], point[1] + center[1]);
    }

    z = first[3];
    depth = getDepth(z);
    ctx.lineWidth = getLineWidth(depth);
    ctx.globalAlpha = getAlpha(depth);

    // Close path
    ctx.lineTo(first[0] + center[0], first[1] + center[1]);
    ctx.stroke();
  }

  function drawPolygons(ctx, polygons, points, center) {
    polygons.forEach(function(polygon) {
      drawPolygon(ctx, polygon, points, center);
    });
  }

  function drawParticle(ctx, particle) {
    ctx.globalAlpha = 0.1;
    ctx.fillStyle = FILL_STYLE;
    ctx.beginPath();
    ctx.arc(particle.x, particle.y, particle.r * 2, 0, 2 * Math.PI);
    ctx.fill();
    ctx.closePath();
  }

  function drawParticleLine(ctx, a, b, d, maxd) {
    ctx.strokeStyle = FILL_STYLE;
    ctx.lineWidth = 1;
    ctx.globalAlpha = 0.1 * (1 - d / maxd);
    ctx.beginPath();
    ctx.moveTo(a.x, a.y);
    ctx.lineTo(b.x, b.y);
    ctx.stroke();
    ctx.closePath();
  }

  return {
    transformPoints: transformPoints,
    prepareModel: prepareModel,
    drawPoints: drawPoints,
    drawPolygons: drawPolygons,
    drawParticle: drawParticle,
    drawParticleLine: drawParticleLine
  };
})();

