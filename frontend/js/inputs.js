var INPUT_CLASS = 'js-input';
// Attach event handlers to inputs
document.addEventListener('change', function(ev) {
  if (ev.target && ev.target.classList.contains(INPUT_CLASS)) {
    var inputEl = ev.target;
    if (inputEl.value.length) {
      inputEl.classList.add('is-active');
    } else {
      inputEl.classList.remove('is-active');
    }
  }
});
