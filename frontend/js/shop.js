/* eslint max-len: [1, 110] */
require('./polyfills');
require('paypal-checkout');

var qs = require('qs');
var OAuth = require('oauth-1.0a');
var CryptoJS = require('crypto-js');
var axios = require('axios');
var localForage = require('localforage');
var delegate = require('dom-delegate');
var $ = require('./helpers').$;
var $$ = require('./helpers').$$;
var Mustache = require('mustache');
var Barba = require('barba.js');

// gneral & flags
var eventDelegate = delegate(document.body);
var updatingView = false;

// elements
var orderForm = '#order-form';
var updateCartBtn = '.update-cart';
var cartTemplateScript = '#cart-template';
var cart = '#cart';
var cartCount = '.cart-count';
var cartTotal = '.cart-total';

// shop credentials
var shopUrlBase = 'http://leclair.ch/wp/wp-json/wc/v1';
var consumerKey = 'ck_88c0422e09a0d852d1ed132131a4380a0f20042c';
  // 'ck_71256c11e268e2fb591837f3b9f4d8a6ea0e0d9f';
var consumerSecret = 'cs_b493fe74438683147b80131a65b2b4ef5757fdcf';
  // 'cs_c583f2b9a59a32114befb45788a54b78261f20f0';
var payPalSandbox = 'AfZ0gPsSmEwfwzXJtRnEKXY7NkVmgOyYGxwY7MfesNTjHtrYOYdL8K673SjbDtmr5ZRbxpWBiIN5BAMF';
// var action2Checkout = 'https://sandbox.2checkout.com/checkout/purchase';
var sid2Checkout = '901342686';
var shippingAmount = '9.99';

// products mockup
var PRODUCTS = [
  {
    product_id: 13,
    title: 'Corrective Cell Definer',
    price: 399.00,
    thumb: './images/product-small-1.png',
    href: 'product-summary-corrective-cell.html'
  },
  {
    product_id: 12,
    title: 'Imperial Youth Serum',
    price: 399.00,
    thumb: './images/product-small-2.png',
    href: 'product-summary-imperial-youth.html'
  },
  {
    product_id: 11,
    title: 'Expert Hydracharge Day Cream',
    price: 399.00,
    thumb: './images/product-small-3.png',
    href: 'product-summary-expert-hydracharge.html'
  },
  {
    product_id: 10,
    title: 'Supreme Eye Elixir',
    price: 399.00,
    thumb: './images/product-small-4.png',
    href: 'product-summary-supreme-eye-elixir.html'
  },
  {
    product_id: 9,
    title: 'Ultimate Rejuvenating Night Cream',
    price: 399.00,
    thumb: './images/product-small-5.png',
    href: 'product-summary-ultimative-rejuvenating.html'
  },
  {
    product_id: 8,
    title: 'Royal Renewal Serum',
    price: 399.00,
    thumb: './images/product-small-6.png',
    href: 'product-summary-royal-renewal.html'
  }
];

// templates
var customTags = ['<%', '%>'];
var cartTemplate = $(cartTemplateScript) ? $(cartTemplateScript).innerHTML : '';

// localForage to manage shop app state
var store = localForage.createInstance({
  driver: localForage.INDEXEDDB,
  name: 'leclairApp',
  storeName: 'leclairShop'
});

// OAuth configure
var oauth = OAuth({
  consumer: {
    key: consumerKey,
    secret: consumerSecret
  },
  signature_method: 'HMAC-SHA1',
  hash_function: function(baseString, key) {
    return CryptoJS.HmacSHA1(baseString, key).toString(CryptoJS.enc.Base64);
  }
});

function handleError(/* err */) {
  // console.debug(err);
}

function serializeForm(form) {
  if (!form || form.nodeName !== 'FORM') {
    return null;
  }
  var i = [];
  var j = [];
  var q = {};
  for (i = form.elements.length - 1; i >= 0; i = i - 1) {
    if (form.elements[i].name === '') {
      continue;
    }
    switch (form.elements[i].nodeName) {
      case 'INPUT':
        switch (form.elements[i].type) {
          case 'text':
          case 'hidden':
          case 'password':
          case 'button':
          case 'reset':
          case 'submit':
            q[form.elements[i].name] = form.elements[i].value;
            break;
          case 'checkbox':
          case 'radio':
            if (form.elements[i].checked) {
              q[form.elements[i].name] = form.elements[i].value;
            }
            break;
          default:
            break;
        }
        break;
      case 'TEXTAREA':
        q[form.elements[i].name] = form.elements[i].value;
        break;
      case 'SELECT':
        switch (form.elements[i].type) {
          case 'select-one':
            q[form.elements[i].name] = form.elements[i].value;
            break;
          case 'select-multiple':
            for (j = form.elements[i].options.length - 1; j >= 0; j = j - 1) {
              if (form.elements[i].options[j].selected) {
                q[form.elements[i].name] = form.elements[i].options[j].value;
              }
            }
            break;
          default:
            break;
        }
        break;
      case 'BUTTON':
        switch (form.elements[i].type) {
          case 'reset':
          case 'submit':
          case 'button':
            q[form.elements[i].name] = form.elements[i].value;
            break;
          default:
            break;
        }
        break;
      default:
        break;
    }
  }
  return q;
}
/**
 * Update products in the store
 * @param state (obj) current store state
 * @param productObject (obj) PRODUCT
 * @param decrement (bool) should quantity ++ || --
 * @param trash (bool) should trash product
 *
 * @return (obj) updated store object
 */
function storeUpdateProducts(state, product, decrement, trash) {
  var i = state.findIndex(function(p) {
    return p.product_id === product.product_id;
  });

  if (i < 0 && (decrement || trash)) {
    return false;
  }

  if (trash) {
    return state.slice(0, i).concat(state.slice(i + 1));
  }

  var updatedProduct = Object.assign({}, product, {
    quantity: (i > -1 ? state[i].quantity : 0) + (decrement ? -1 : 1)
  });

  switch (updatedProduct.quantity) {
    case -1:
    case 0:
      return state.slice(0, i).concat(state.slice(i + 1));
    case 1:
      return !decrement ?
        state.concat([updatedProduct]) :
        state.slice(0, i).concat([updatedProduct], state.slice(i + 1));
    default:
      return state.slice(0, i).concat([updatedProduct], state.slice(i + 1));
  }
}

function calculateChart(lineItems, shipping) {
  var items = lineItems || [];
  return {
    items: items.map(function(i) {
      return Object.assign({}, i, {
        price: i.price.toFixed(2),
        subtotal: (i.quantity * i.price).toFixed(2)
      });
    }),
    total: items.reduce(function(p, n) {
      return p + (n.quantity * n.price);
    }, items.length ? shipping : 0).toFixed(2),
    shipping: shipping.toFixed(2)
  };
}

function updateView(lineItems) {
  if (!Barba.Pjax.transitionProgress &&
    (!lineItems || !lineItems.length) && $(cart)) {
    updatingView = false;
    return Barba.Pjax.goTo('empty-bag.html');
  }
  var cartData = calculateChart(lineItems, parseFloat(shippingAmount));
  if ($$(cartTotal)) {
    $$(cartTotal).forEach(function(elem) {
      elem.innerHTML = '$' + cartData.total;
    });
  }
  if ($$(cartCount)) {
    $$(cartCount).forEach(function(elem) {
      elem.innerHTML = cartData.items.reduce(function(p, n) {
        return p + n.quantity;
      }, 0);
      // elem.innerHTML = cartData.items.length;
    });
  }
  if ($(cart)) $(cart).innerHTML = Mustache.render(cartTemplate, cartData);
  updatingView = false;
}

function createOrder(orderData, handleErrFn) {
  if (updatingView) return false;
  updatingView = true;
  var orderRequest = {
    url: shopUrlBase + '/orders',
    method: 'POST'
  };
  var orderParams = qs.stringify(oauth.authorize(orderRequest));

  axios({
    method: orderRequest.method,
    url: orderRequest.url + '?' + orderParams,
    data: qs.stringify(orderData),
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  })
  .then(function() {
    return store.clear();
  })
  .then(function() {
    updatingView = false;
    Barba.Pjax.goTo('checkout-thank-you.html');
  })
  .catch(handleErrFn);
}

/**
 * Format form data for later usage
 * @param d (obj) form data
 */
function validateFormData(d) {
  var res = {e: [], data: Object.assign({}, d)};
  if (d.first_name === '') res.e.push('first_name');
  if (d.last_name === '') res.e.push('last_name');
  if (d.email === '') res.e.push('email');
  if (d.phone === '') res.e.push('phone');

  // billing
  if (d.line1 === '') res.e.push('line1');
  if (d.postal_code === '') res.e.push('postal_code');
  if (d.city === '') res.e.push('city');
  if (d.country_code === '') res.e.push('country_code');
  if (d.state === '') res.e.push('state');

  // shipping
  var bill = d.hasOwnProperty('same_as_billing');
  if (!bill) {
    if (d.shipping_line1 === '') res.e.push('shipping_line1');
    if (d.shipping_postal_code === '') res.e.push('shipping_postal_code');
    if (d.shipping_city === '') res.e.push('shipping_city');
    if (d.shipping_country_code === '') res.e.push('shipping_country_code');
    if (d.shipping_state === '') res.e.push('shipping_state');
  }
  res.data.shipping = {
    line1: bill ? d.line1 : d.shipping_line1,
    postal_code: bill ? d.postal_code : d.shipping_postal_code,
    city: bill ? d.city : d.shipping_city,
    country_code: bill ? d.country_code : d.shipping_country_code,
    state: bill ? d.state : d.shipping_state
  };
  return res;
}

function highlightFormErrors(errors) {
  // console.log(errors);
  var toggleClass = function(e) {
    var elem = e.target;
    var parent = elem.parentElement;
    if (elem.value && elem.value !== '') {
      parent.classList.remove('has-error');
    } else {
      parent.classList.add('has-error');
    }
  };
  $$('input').forEach(function(inputEl) {
    inputEl.parentElement.classList.remove('has-error');
    inputEl.removeEventListener('keyup', toggleClass);
  });
  $$('select').forEach(function(inputEl) {
    inputEl.parentElement.classList.remove('has-error');
    inputEl.removeEventListener('change', toggleClass);
  });
  if (!errors || !errors.length || !Array.isArray(errors)) {
    return false;
  }
  errors.forEach(function(fieldName) {
    $$('input[name="' + fieldName + '"]').forEach(function(elem) {
      elem.parentElement.classList.add('has-error');
      elem.addEventListener('keyup', toggleClass);
    });
    $$('select[name="' + fieldName + '"]').forEach(function(elem) {
      elem.parentElement.classList.add('has-error');
      elem.addEventListener('change', toggleClass);
    });
  });
}

function formatOrderData(rawData) {
  var result = {
    set_paid: true,
    shipping_lines: [
      {
        method_id: 'flat_rate',
        method_title: 'Flat Rate',
        total: parseFloat(shippingAmount)
      }
    ]
  };
  var isPaypalResponse = rawData.hasOwnProperty('payer');
  var payerInfo = isPaypalResponse ? rawData.payer.payer_info : rawData;
  var paypalBilling = payerInfo.hasOwnProperty('billing_address') ?
    payerInfo.billing_address : payerInfo.shipping_address;
  var billing = isPaypalResponse ? paypalBilling : rawData;
  var shipping = isPaypalResponse ?
    payerInfo.shipping_address : rawData.shipping;
  var transaction = rawData.transactions &&
    rawData.transactions.length ? rawData.transactions[0] : false;
  var lineItems = transaction ? transaction.item_list.items.map(function(i) {
    return {product_id: i.sku, quantity: i.quantity};
  }) : rawData.lineItems;

  if (isPaypalResponse && transaction && transaction.related_resources) {
    result.transaction_id = transaction.related_resources[0].sale.id;
  }

  return Object.assign({}, result, {
    payment_method: isPaypalResponse ? 'paypal' : 'twocheckout',
    payment_method_title: isPaypalResponse ? 'PayPal' : 'Direct Bank Transfer',
    billing: {
      first_name: payerInfo.first_name,
      last_name: payerInfo.last_name,
      address_1: billing.line1,
      city: billing.city,
      state: billing.state,
      postcode: billing.postal_code,
      country: billing.country_code,
      email: payerInfo.email,
      phone: payerInfo.phone
    },
    shipping: {
      first_name: payerInfo.first_name,
      last_name: payerInfo.last_name,
      address_1: shipping.line1,
      city: shipping.city,
      state: shipping.state,
      postcode: shipping.postal_code,
      country: shipping.country_code
    },
    line_items: lineItems
  });
}

function formatPayPalPaymentData(lineItems) {
  var reducer = function(p, n) {
    return p + (n.price * n.quantity);
  };
  return {
    intent: 'sale',
    transactions: [{
      amount: {
        total: lineItems.reduce(reducer, parseFloat(shippingAmount)).toFixed(2),
        currency: 'USD',
        details: {
          shipping: shippingAmount,
          tax: '0',
          subtotal: lineItems.reduce(reducer, 0).toFixed(2)
        }
      },
      item_list: {
        items: lineItems.map(function(i) {
          return {
            name: i.title,
            sku: i.product_id,
            quantity: i.quantity,
            price: i.price.toFixed(2),
            currency: 'USD'
          };
        })
      }
    }]
  };
}

function payPalBtn() {
  return {
    env: 'sandbox',
    client: {
      sandbox: payPalSandbox,
      production: ''
    },
    payment: function() {
      var env    = this.props.env;
      var client = this.props.client;

      return store.getItem('line_items')
        .then(function(lineItems) {
          return window.paypal.rest.payment.create(
            env,
            client,
            formatPayPalPaymentData(lineItems)
          );
        });
    },
    commit: true,
    onAuthorize: function(data, actions) {
      return actions.payment.execute().then(function(resdata) {
        // console.log(resdata);
        createOrder(formatOrderData(resdata), handleError);
      });
    },
    style: {
      size: 'medium',
      color: 'orange',
      shape: 'rect'
    }
  };
}

function handleUpdateCart(e) {
  if (updatingView) return false;
  updatingView = true;

  var elem = this;
  var productID = parseInt(elem.dataset.productId, 10);
  var currentProduct = PRODUCTS.find(function(product) {
    return product.product_id === productID;
  });
  if (!currentProduct) {
    updatingView = false;
    return null;
  }
  var decrementAction = elem.dataset.action === 'remove';
  var trashAction = elem.dataset.trash === 'true';
  var toggle = elem.dataset.toggleQuantity === 'true';
  store.getItem('line_items')
    .then(function(lineItems) {
      var items = lineItems || [];
      var u = storeUpdateProducts(
        items,
        currentProduct,
        decrementAction,
        trashAction
      ) || [];
      return store.setItem('line_items', u);
    })
    .then(function() {
      if (toggle || trashAction) {
        store.getItem('line_items')
          .then(updateView)
          .catch(handleError);
      } else {
        Barba.Pjax.goTo(currentProduct.href);
      }
    })
    .catch(handleError);
  return e.preventDefault();
}

function handleSubmitOrder(e) {
  var orderData = serializeForm(this);
  var validated = validateFormData(orderData);
  if (validated.e.length > 0) {
    highlightFormErrors(validated.e);
    return e.preventDefault();
  }
  store.getItem('line_items')
  .then(function(lineItems) {
    var formatedData = formatOrderData(Object.assign(
      {lineItems: lineItems},
      validated.data
    ));
    // console.log(formatedData);
    // save formated data
    return store.setItem('formatedData', formatedData);
  })
  .then(function(formatedData) {
    // create and submit 2co-form
    var form2co = document.getElementById('form-2co');
    var submitBtn = document.getElementById('form-2co-submit');
    var elementSet = function(element) {
      if (document.getElementById(element.name)) {
        document.getElementById(element.name).setAttribute('value', element.value);
        return;
      }
      var input = document.createElement('input');
      input.setAttribute('type', 'hidden');
      input.setAttribute('name', element.name);
      input.setAttribute('id', element.name);
      input.setAttribute('value', element.value);
      form2co.appendChild(input);
    };

    // while (form2co.hasChildNodes()) {
    //   form2co.removeChild(form2co.lastChild);
    // }

    // form2co.setAttribute('method', 'post');
    // form2co.setAttribute('action', action2Checkout);

    var formItems = [
      {
        name: 'sid',
        value: sid2Checkout
      },
      {
        name: 'mode',
        value: '2CO'
      },
      {
        name: 'email',
        value: formatedData.billing.email
      },
      {
        name: 'phone',
        value: formatedData.billing.phone
      },
      {
        name: 'card_holder_name',
        value: formatedData.billing.first_name + ' ' + formatedData.billing.last_name
      },
      {
        name: 'street_address',
        value: formatedData.billing.address_1
      },
      {
        name: 'street_address2',
        value: ''
      },
      {
        name: 'city',
        value: formatedData.billing.city
      },
      {
        name: 'state',
        value: formatedData.billing.state
      },
      {
        name: 'country',
        value: formatedData.billing.country
      },
      {
        name: 'zip',
        value: formatedData.billing.postcode
      },
      {
        name: 'ship_name',
        value: formatedData.billing.first_name + ' ' + formatedData.billing.last_name
      },
      {
        name: 'ship_street_address',
        value: formatedData.shipping.address_1
      },
      {
        name: 'ship_street_address2',
        value: ''
      },
      {
        name: 'ship_city',
        value: formatedData.shipping.city
      },
      {
        name: 'ship_state',
        value: formatedData.shipping.state
      },
      {
        name: 'ship_zip',
        value: formatedData.shipping.postcode
      },
      {
        name: 'ship_country',
        value: formatedData.shipping.country
      }
    ];

    var formLineItems = formatedData.line_items.map(function(item, index) {
      return [
        {
          name: 'li_' + index + '_type',
          value: 'product'
        },
        {
          name: 'li_' + index + '_tangible',
          value: 'Y'
        },
        {
          name: 'li_' + index + '_name',
          value: item.title
        },
        {
          name: 'li_' + index + '_price',
          value: item.price
        },
        {
          name: 'li_' + index + '_quantity',
          value: item.quantity
        }
      ];
    });

    var shippingItem = [
      {
        name: 'li_' + formLineItems.length + '_type',
        value: 'shipping'
      },
      {
        name: 'li_' + formLineItems.length + '_name',
        value: 'Flat Rate'
      },
      {
        name: 'li_' + formLineItems.length + '_price',
        value: shippingAmount
      }
    ];

    formItems.forEach(elementSet, this);

    formLineItems.forEach(function(element) {
      element.forEach(elementSet);
    }, this);

    shippingItem.forEach(elementSet, this);

    // var submitBtn = document.createElement('input');
    // submitBtn.setAttribute('name', 'submit');
    // submitBtn.setAttribute('type', 'submit');
    // submitBtn.setAttribute('value', 'submit');
    // form2co.appendChild(submitBtn);

    // document.getElementsByTagName('body')[0].appendChild(form2co);
    submitBtn.click();

    // document.getElementsByTagName('body')[0].appendChild(form2co);
    // createOrder(formatedData, handleError);
  });
  return e.preventDefault();
}

/**
 * Render and bind event listeners
 */
function shopInit() {
  Mustache.parse(cartTemplate, customTags);

  store.getItem('line_items')
    .then(updateView)
    .catch(handleError);

  // attach event listeners
  eventDelegate.on(
    'click',
    updateCartBtn,
    handleUpdateCart
  );
  eventDelegate.on('submit', orderForm, handleSubmitOrder);

  if ($('#paypal-button')) {
    window.paypal.Button
    .render(payPalBtn(), '#paypal-button');
  }

  // qs test
  if (typeof window !== 'undefined' && location.search) {
    var queryString = location.search.indexOf('?') === 0 ?
      location.search.substr(location.search.indexOf('?') + 1) :
      location.search;
    var response = qs.parse(queryString);
    if (response.sid !== sid2Checkout) {
      return;
    }
    store.getItem('formatedData')
      .then(function(formatedData) {
        createOrder(formatedData, handleError);
        store.removeItem('formatedData')
          .then(function() {
            // console.log('store formatedData cleared');
          });
      });
  }
}


// initializes
shopInit();
Barba.Dispatcher.on('newPageReady', shopInit);
Barba.Dispatcher.on('transitionCompleted', function() {
  if (!$(orderForm) && !$(cart)) {
    return;
  }
  store.getItem('line_items')
  .then(function(items) {
    if (!items || !items.length) {
      updatingView = false;
      if ($(orderForm)) {
        return Barba.Pjax.goTo('checkout-thank-you.html');
      }
      if ($(cart)) {
        return Barba.Pjax.goTo('empty-bag.html');
      }
    }
  })
  .catch(handleError);
});

