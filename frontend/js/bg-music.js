var Howl = require('howler').Howl;
var delegate = require('dom-delegate');

var eventDelegate = delegate(document.body);

var music = new Howl({
  src: ['/sound/bg-music.mp3'],
  autoplay: true,
  loop: true,
  html5: true,
  volume: 0.5
});

var playing = true;

// Add event handlers to toggle sound
eventDelegate.on('click', '.js-bg-music', function(e) {
  e.preventDefault();
  var equalizer = document.querySelector('.js-bg-music');

  if (playing) {
    music.pause();
    equalizer.classList.remove('is-playing');
    playing = false;
  } else {
    !music.playing() && music.play();
    equalizer.classList.add('is-playing');
    playing = true;
  }
});

document.addEventListener('visibilitychange', function() {
  if (document.visibilityState === 'visible') {
    playing && !music.playing() && music.play();
  } else {
    music.pause();
  }
});
