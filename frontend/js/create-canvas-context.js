var createContext = require('2d-context');
var CANVAS_SELECTOR = '.js-canvas';


module.exports = (function() {
  var canvasEl = document.querySelector(CANVAS_SELECTOR);
  var ctx = createContext({ canvas: canvasEl });

  return ctx;
})();
