var Barba = require('barba.js');
var Promise = require('bluebird');
var delegate = require('dom-delegate');
var animateElements = require('./cssanims').animateElements;
var animateIn = require('./cssanims').animateIn;
var animateOut = require('./cssanims').animateOut;
var clearAnimationOnElements = require('./cssanims').clearAnimationOnElements;
var classNames = require('./class-names');
var changeAnimation = require('./canvas-animations').changeAnimation;
var view = require('./view');
var $ = require('./helpers').$;
var $$ = require('./helpers').$$;

var header = $('.js-header');
var footer = $('.js-footer');

var PageTransition = Barba.BaseTransition.extend({
  start: function() {
    /**
     * This function is automatically called as soon the Transition starts
     * this.newContainerLoading is a Promise for the loading of the
     * new container
     */

    // As soon the loading is finished and the old page is faded out,
    // let's animate in the new page
    this.newContainerLoading
      .then(this.animateOut.bind(this))
      .then(this.animateIn.bind(this));
  },

  headerIn: function() {
    return header.classList.contains(classNames.hidden);
  },

  animateOut: function() {
    var canvasEl = $('.js-canvas');

    if (this.newContainer.dataset.noHeader.length > 0 && !this.headerIn()) {
      animateOut(header);
      animateOut(footer);
    }

    // Remove default Barba.js styling
    this.oldContainer.removeAttribute('style');
    // Set new container height to 0 to prevent content jumping
    this.newContainer.style.height = 0;
    this.newContainer.style.overflow = 'hidden';

    // Clear all animation inside old container before proceeding
    clearAnimationOnElements($$('.js-animated', this.oldContainer));

    // Fade out canvas
    return Promise.all([animateOut(canvasEl), animateOut(this.oldContainer)]);
  },

  animateIn: function() {
    var _this = this;
    var canvasEl = $('.js-canvas');
    var el = this.newContainer;
    el.classList.add(classNames.hidden);
    // Remove default Barba.js styling
    el.removeAttribute('style');

    // Get new canvas animation
    var canvasAnim = el.dataset.canvasAnim;

    // Show header/footer
    if (!el.dataset.noHeader.length > 0 && this.headerIn()) {
      animateIn(header);
      animateIn(footer);
    }

    return Promise
      .all([
        animateIn(el),
        animateIn(canvasEl),
        animateElements(el),
        changeAnimation(canvasAnim)
      ])
      .then(function() { _this.done(); });
  }
});

Barba.Pjax.getTransition = function() {
  return PageTransition;
};

var View = Barba.BaseView.extend(view);

View.init();

Barba.Pjax.start();

var eventDelegate = delegate(document.body);
eventDelegate.on('click', 'a', function(e) {
  var current = Barba.Utils.getCurrentUrl();
  var next = this.href;
  // If next address is current address or transition is in progress
  if (next === current || Barba.Pjax.transitionProgress) {
    // Ignore clicks
    e.preventDefault();
    e.stopPropagation();
  }
});

global.Barba = Barba;
