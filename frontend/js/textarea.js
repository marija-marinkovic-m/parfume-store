var debounce = require('lodash.debounce');
var throttle = require('lodash.throttle');

var TEXTAREA_CLASS = 'js-textarea';
var OVERFLOW_CLASS = 'is-overflow';

function resizeTextarea(el) {
  var scrollHeight = 0;
  var maxHeight = parseInt(getComputedStyle(el).maxHeight, 10);
  var classListOperation = 'remove';

  // Calculate new scroll height
  el.style.height = '';
  scrollHeight = el.scrollHeight;

  if (scrollHeight > el.clientHeight) {
    el.style.height = scrollHeight + 'px';
  }

  // Add/remove overflow
  if (scrollHeight > maxHeight) classListOperation = 'add';
  el.classList[classListOperation](OVERFLOW_CLASS);
}

function onInput(ev) {
  if (ev.target && ev.target.classList.contains(TEXTAREA_CLASS)) {
    resizeTextarea(ev.target);
  }
}

function onResize() {
  var textareaNodes = document.querySelectorAll('.' + TEXTAREA_CLASS);
  if (!textareaNodes.length) return;
  var textareaElements = Array.prototype.slice.apply(textareaNodes);
  textareaElements.forEach(resizeTextarea);
}

// Attach event handlers to inputs
document.addEventListener('input', throttle(onInput, 50));
document.addEventListener('paste', throttle(onInput, 50));
document.addEventListener('keyup', throttle(onInput, 50));
window.addEventListener('resize', debounce(onResize, 100));
