var createContext = require('2d-context');
var mat4 = require('gl-mat4');
var sphere = require('primitive-sphere');
var transformPoints = require('./drawing').transformPoints;
var prepareModel = require('./drawing').prepareModel;
var drawPolygons = require('./drawing').drawPolygons;

var ROTATE_SPEED = 0.0002;

module.exports = (function() {
  // Initialize mesh
  var center = [];
  var width = 0;
  var height = 0;
  var sphereModel = sphere(1, { segments: 6 });
  var rotateMatrix = mat4.create();
  var pvMatrix = mat4.create();
  var ctx = {};
  prepareModel(sphereModel, 15);

  function getPVMatrix() {
    // Prepare view matrix
    var viewMatrix = mat4.lookAt([],
      // Camera position
      [0, 10, 40],
      // Target position
      [0, 0, 0],
      // Camera orientation
      [0, 1, 0]
    );

    // Prepare projection matrix
    var projectionMatrix = mat4.perspective([],
      // Camera angle
      Math.PI / 20,
      // Aspect ratio
      1,
      // Near plane
      0,
      // Far plane
      10
    );

    return mat4.multiply([], projectionMatrix, viewMatrix);
  }

  // Update mesh
  function update(dt) {
    var transformMatrix = mat4.create();
    var scaleMatrix = mat4.create();
    var k = width / 400; // Scale factor

    // Update rotate matrix
    mat4.rotateY(rotateMatrix, rotateMatrix, ROTATE_SPEED * dt);

    // Apply scale transformation
    mat4.scale(scaleMatrix, rotateMatrix, [k, k, k]);

    // Multiply with projection matrix
    mat4.multiply(transformMatrix, pvMatrix, scaleMatrix);

    // Apply transformations
    var positions = transformPoints(sphereModel.positions, transformMatrix);

    sphereModel.positions2d = positions;
  }

  // Draw mesh
  function draw() {
    // Clear canvas
    ctx.clearRect(0, 0, width, height);
    drawPolygons(ctx, sphereModel.cells, sphereModel.positions2d, center);
  }

  // onTick
  function onTick(dt) {
    width = ctx.canvas.width;
    height = ctx.canvas.height;

    // Get new canvas center
    center = [width / 2, height / 2];

    update(dt);
    draw();
  }

  var Constructor = function(el) {
    pvMatrix = getPVMatrix();
    ctx = createContext({ canvas: el });
    this.canvasLoop = require('canvas-loop')(ctx.canvas);
    this.canvasLoop.on('tick', onTick);
    this.canvasLoop.start();
  };

  Constructor.prototype.destroy = function() {
    ctx.clearRect(0, 0, width, height);
    this.canvasLoop.stop();
  };

  return Constructor;
})();
