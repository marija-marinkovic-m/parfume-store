var delegate = require('dom-delegate');
var trigger = require('dispatch-event');
var $ = require('./helpers').$;
var classNames = require('./class-names');

var eventDelegate = delegate(document.body);

// Attach event handlers
eventDelegate.on('click', '.js-tabs', function(ev) {
  if (!ev.target.classList.contains('js-tabs__link')) return;

  ev.preventDefault();

  var newLink = ev.target;
  var oldLink = $('.js-tabs__link.' + classNames.active, this);
  var newTarget = this.querySelector(ev.target.getAttribute('href'));
  var oldTarget = $('.js-tabs__target.' + classNames.active, this);

  oldLink.classList.remove(classNames.active);
  oldTarget.classList.remove(classNames.active);
  newLink.classList.add(classNames.active);
  newTarget.classList.add(classNames.active);

  trigger(window, 'resize'); // Force layout reflow
});
