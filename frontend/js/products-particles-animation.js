var ctx = require('./create-canvas-context');
var drawParticle = require('./drawing').drawParticle;
var drawParticleLine = require('./drawing').drawParticleLine;
var sign = require('./helpers').sign;
var hypot = require('./helpers').hypot;
var MAX_DISTANCE = 100;
var MIN_RADIUS = 0.5;
var MAX_RADIUS = 1.1;
var MAX_SPEED = 0.02;
var MIN_SPEED = 0.003;
var MAX_PARTICLES = 500;
var PARTICLES_PER_PIXEL = 0.013;

module.exports = (function() {
  var width = ctx.canvas.width;
  var height = ctx.canvas.height;
  // Create empty particle and lines array
  var particles = Array.apply(null, Array(MAX_PARTICLES));
  var lines = [];

  function getMaxParticles() {
    var max = (width * height) * (PARTICLES_PER_PIXEL * PARTICLES_PER_PIXEL);
    max = max > MAX_PARTICLES ? MAX_PARTICLES : max;
    return Math.round(max);
  }

  var maxParticles = getMaxParticles();

  // Create particle
  function seedParticle() {
    var particle = {};
    particle.x = Math.floor(Math.random() * width);
    particle.y = Math.floor(Math.random() * height);

    particle.dx = -MAX_SPEED + Math.random() * MAX_SPEED * 2;
    if (Math.abs(particle.dx) < MIN_SPEED) {
      particle.dx = sign(particle.dx) * MIN_SPEED;
    }

    particle.dy = -MAX_SPEED + Math.random() * MAX_SPEED * 2;
    if (Math.abs(particle.dy) < MIN_SPEED) {
      particle.dy = sign(particle.dy) * MIN_SPEED;
    }

    particle.r = MIN_RADIUS + Math.random() * (MAX_RADIUS - MIN_RADIUS);

    return particle;
  }

  // Seed particles
  function seedParticles() {
    particles = particles.map(function(el, idx) {
      // Check if there is enough particles already
      if (idx > maxParticles) return undefined;
      return seedParticle();
    }).filter(function(el) { return el; });
  }

  // Sort particles
  function sortParticles(a, b) {
    return a.x < b.x ? 1 : -1;
  }

  function updateParticleResolution() {
    maxParticles = getMaxParticles();
    var num = particles.length;
    if (num === maxParticles) return;
    if (num > maxParticles) {
      // Remove excess particles
      particles.splice(maxParticles);
    } else {
      var newParticles =
        Array.apply(null, Array(maxParticles - num)).map(seedParticle);
      // Merge newParticles with particles array
      Array.prototype.push.apply(particles, newParticles);
    }
  }

  // Find close particles and fill lines array
  function findLines() {
    // Reset lines
    lines = [];

    // Start with first particle
    // Check if next X is close
    // no: start from next particle
    // yes
    // check if distance is ok
    // yes: write line
    // check next particle
    var a = 0;
    var b = 1;
    while (a < particles.length && b < particles.length) {
      if (particles[b].x - particles[a].x > MAX_DISTANCE) {
        a = a + 1;
        if (a === b) b = a + 1;
        continue;
      }
      var d = hypot(
        particles[a].x, particles[a].y,
        particles[b].x, particles[b].y
      );
      if (d < MAX_DISTANCE) lines.push([a, b, d]);
      b = b + 1;
      if (b === particles.length) {
        a = a + 1;
        b = a + 1;
      }
    }
  }


  // Update positions
  function moveParticle(particle, dt) {
    particle.x += particle.dx * dt;
    if (particle.x - particle.r > width) particle.x = 0;
    if (particle.x + particle.r < 0) particle.x = width - particle.r;

    particle.y += particle.dy * dt;
    if (particle.y - particle.r > height) particle.y = 0;
    if (particle.y + particle.r < 0) particle.y = height - particle.r;
  }

  function update(dt) {
    updateParticleResolution();

    // Move particles
    particles.forEach(function(particle) {
      moveParticle(particle, dt);
    });

    // Resort particles
    particles.sort(sortParticles);

    // Recalculate lines
    findLines();
  }


  function draw() {
    ctx.clearRect(0, 0, width, height);
    particles.forEach(function(particle) {
      drawParticle(ctx, particle);
    });
    lines.forEach(function(line) {
      var a = particles[line[0]];
      var b = particles[line[1]];
      var d = line[2];

      drawParticleLine(ctx, a, b, d, MAX_DISTANCE);
    });
  }

  function onTick(dt) {
    width = ctx.canvas.width;
    height = ctx.canvas.height;

    update(dt);
    draw();
  }

  // Init
  seedParticles();

  return {
    onTick: onTick
  };
})();

