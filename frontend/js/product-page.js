var delegate = require('dom-delegate');
var trigger = require('dispatch-event');
var classnames = require('./class-names');
var animateElements = require('./cssanims').animateElements;
var transitionEnd = require('./helpers').getTransitionEndEvent();
var $ = require('./helpers').$;
var carousel = require('./carousel');
var eventDelegate = delegate(document.body);

function openModal() {
  var ingredientsModal = $('.js-ingredients-modal');
  if (ingredientsModal.classList.contains(classnames.active)) return;

  // Show modal
  ingredientsModal.classList.add(classnames.active);

  // Change size of current section
  $('.js-product-section.' + classnames.active)
    .classList.add(classnames.modalOpened);

  // Change size of ingredients part of section
  $('.js-ingredients').classList.add(classnames.modalOpened);

  // Prevent content scrolling under modal window
  document.documentElement.classList.add('is-not-scrollable');

  ingredientsModal.addEventListener(transitionEnd, function onTransitionEnd() {
    setTimeout(function() {
      trigger(window, 'resize');
      this.removeEventListener(transitionEnd, onTransitionEnd);
    }, 100); // Transition end fires few milliseconds earlier
  });
}

function closeModal() {
  var ingredientsModal = $('.js-ingredients-modal');
  if (!ingredientsModal.classList.contains(classnames.active)) return;

  // Hide modal
  ingredientsModal.classList.remove(classnames.active);

  // Revert size of current section
  var section = $('.js-product-section.' + classnames.modalOpened);
  section && section.classList.remove(classnames.modalOpened);

  // Revert size of ingredients part of section
  $('.js-ingredients').classList.remove(classnames.modalOpened);

  // Clear active ingredient
  var activeIngredient = $('.js-ingredient.' + classnames.active);
  activeIngredient && activeIngredient.classList.remove(classnames.active);

  document.documentElement.classList.remove('is-not-scrollable');

  ingredientsModal.addEventListener(transitionEnd, function onTransitionEnd() {
    setTimeout(function() {
      trigger(window, 'resize');
      this.removeEventListener(transitionEnd, onTransitionEnd);
    }, 100); // Transition end fires few milliseconds earlier
  });
}

function changeModalContents(target) {
  var ingredientsModalContent = $('.js-ingredients-modal__content');
  ingredientsModalContent.innerHTML = target;
  carousel.init(ingredientsModalContent);
}

eventDelegate.on(
  'click',
  '.js-section-nav-link:not(.' + classnames.active + ')',
  function() {
    var target = $(this.getAttribute('href'));
    if (!target) return;

    // Set active nav link
    $('.js-section-nav-link.' + classnames.active)
      .classList.remove(classnames.active);
    this.classList.add(classnames.active);

    // Set active section
    $('.js-product-section.' + classnames.active)
      .classList.remove(classnames.active);
    target.classList.add(classnames.active);

    // Animate elements in section
    animateElements(target);

    closeModal();

    trigger(window, 'resize');
  }
);

eventDelegate.on('click', '.js-ingredient', function() {
  // Set active ingredient
  var activeIngredient = $('.js-ingredient.' + classnames.active);
  activeIngredient && activeIngredient.classList.remove(classnames.active);
  this.classList.add(classnames.active);

  var target = $(this.getAttribute('href'));
  if (target) changeModalContents(target.innerHTML);
  openModal();

  // Hide section nav
  $('.js-section-nav').style.visibility = 'hidden';
});

eventDelegate.on('click', '.js-close', function() {
  closeModal();

  // Show section nav
  $('.js-section-nav').style.visibility = '';
});
