var Promise = require('bluebird');

module.exports = (function() {
  var ctx = require('./create-canvas-context');
  var canvasLoop = require('./canvas-loop')(ctx.canvas);
  var playing = false;
  var animations = {};
  animations.intro = require('./intro-spheres-animation').onTick;
  animations.products = require('./products-particles-animation').onTick;
  animations.product = require('./product-canvas-anim').onTick;

  var currentAnimation = null;

  function stopAnimation() {
    if (!playing) return Promise.resolve();
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    playing = false;
    return Promise.resolve(canvasLoop.stop());
  }

  function startNewAnimation(newAnimation) {
    canvasLoop.stop();
    if (currentAnimation) canvasLoop.removeListener('tick', currentAnimation);
    currentAnimation = newAnimation;
    canvasLoop.on('tick', currentAnimation);
    playing = true;
    return Promise.resolve(canvasLoop.start());
  }

  function changeAnimation(animation) {
    var newAnimation = animations[animation];
    if (newAnimation) {
      return Promise.resolve(startNewAnimation(newAnimation));
    }

    return Promise.resolve(stopAnimation());
  }

  document.addEventListener('visibilitychange', function() {
    if (document.visibilityState === 'visible') {
      playing && canvasLoop.start();
    } else {
      canvasLoop.stop();
    }
  });

  return {
    startNewAnimation: startNewAnimation,
    changeAnimation: changeAnimation
  };
})();


