var delegate = require('dom-delegate');
var $ = require('./helpers').$;
var animateIn = require('./cssanims').animateIn;
var animateOut = require('./cssanims').animateOut;
var animateElements = require('./cssanims').animateElements;

var NAV = '.js-nav';
var NAV_LINKS = '.js-nav a';
var NAV_BUTTON = '.js-nav-button';
var NAV_CLOSE = '.js-nav-close';

var eventDelegate = delegate(document.body);

function openMenu() {
  var buttonEl = $(NAV_BUTTON);
  var navEl = $(NAV);
  buttonEl.classList.toggle('is-open');
  animateIn(navEl);
  animateElements(navEl);
  document.documentElement.classList.add('is-not-scrollable');
}

function closeMenu() {
  var navEl = $(NAV);
  animateOut(navEl);
  navEl.classList.toggle('is-open');
  document.documentElement.classList.remove('is-not-scrollable');
}

// Attach event handlers
eventDelegate.on('click', NAV_BUTTON, openMenu);

eventDelegate.on('click', NAV_CLOSE, closeMenu);

eventDelegate.on('click', NAV_LINKS, closeMenu);

