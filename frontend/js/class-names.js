module.exports = {
  active: 'is-active',
  hidden: 'u-visuallyhidden',
  transparent: 'u-transparent',
  animated: 'animated',
  animating: 'is-animating',
  modalOpened: 'is-modal-open'
};
