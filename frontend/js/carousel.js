var Swiper = require('swiper');
var $ = require('./helpers').$;
var $$ = require('./helpers').$$;

module.exports = {
  init: function(el) {
    var carousels = $$('.js-carousel', el);
    if (!carousels.length) return;

    carousels.forEach(function(carousel) {
      /* eslint no-new: 0 */
      var pagination = $('.js-carousel__pagination', carousel);
      var prevButton = $('.js-carousel__prev', carousel);
      var nextButton = $('.js-carousel__next', carousel);

      new Swiper(carousel, {
        pagination: pagination,
        prevButton: prevButton,
        nextButton: nextButton,
        buttonDisabledClass: 'is-disabled',
        paginationType: 'fraction',
        paginationModifierClass: '',
        slidesPerView: 'auto'
      });
    });
  },

  destroy: function(el) {
    var carousels = $$('.js-carousel', el);
    if (!carousels.length) return;

    carousels.forEach(function(carousel) {
      carousel.swiper.destroy();
    });
  }
};

