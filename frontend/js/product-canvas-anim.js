var mat4 = require('gl-mat4');
var vec4 = require('gl-vec4');
var marchingTetrahedra = require('isosurface').marchingTetrahedra;
var mergeVertices = require('merge-vertices');
// Module fast-simplex-noise is exported for es6
var FastSimplexNoise = require('fast-simplex-noise').default;
var ctx = require('./create-canvas-context');
var transformPoints = require('./drawing').transformPoints;
var prepareModel = require('./drawing').prepareModel;
var drawPoints = require('./drawing').drawPoints;
var drawPolygons = require('./drawing').drawPolygons;

var ROTATE_SPEED = 0.00004;
var RANDOMIZE_SPEED = 0.002;
var RANDOMIZE_AMPLITUDE = 0.046;

module.exports = (function() {
  var center = [];
  var width = ctx.canvas.width;
  var height = ctx.canvas.height;
  var model = {};
  var timeStarted = Date.now();
  var rotateMatrix = mat4.create();
  var pvMatrix = mat4.create();

  function generateModel() {
    // Create noise
    var noiseGen = new FastSimplexNoise({
      max: 0.6,
      min: -0.6
    });

    // Create mesh using isosurface function
    var isosurface = marchingTetrahedra(
      // Resolution
      [7, 11, 7],
      // Model generation function
      function(x, y, z) {
        return z * z + x * x - 0.8 - noiseGen.scaled([x, y, z]);
      },
      // Boundaries
      [[-2, -5, -2], [2, 5, 2]]
    );

    // Merge duplicate vertices
    var mesh = mergeVertices(isosurface.cells, isosurface.positions);

    // Randomize y values
    mesh.positions.forEach(function(position) {
      position[1] = position[1] + Math.random() * 0.1;
    });

    // Scale model
    prepareModel(mesh, 70);

    return mesh;
  }

  function getPVMatrix() {
    // Prepare view matrix
    var viewMatrix = mat4.lookAt([],
      // Camera position
      [0, 0, 190],
      // Target position
      [0, 0, 0],
      // Camera orientation
      [1, 5, 0]
    );

    // Prepare projection matrix
    var projectionMatrix = mat4.perspective([],
      // Camera angle
      Math.PI / 4,
      // Aspect ratio
      1,
      // Near plane
      0,
      // Far plane
      10
    );

    return mat4.multiply([], projectionMatrix, viewMatrix);
  }

  function update(dt) {
    var elapsed = Date.now() - timeStarted;
    var length = model.positions.length;
    var transformMatrix = mat4.create();

    // Pseudo-randomize individual points
    var positions = model.positions.map(function(position, idx) {
      var phase = 360 / length * idx;
      var angle = RANDOMIZE_SPEED * elapsed % 360;

      return vec4.scale([], position,
        1 + RANDOMIZE_AMPLITUDE * Math.sin((phase + angle) * Math.PI / 180)
      );
    });

    // Update rotate matrix
    mat4.rotateY(rotateMatrix, rotateMatrix, ROTATE_SPEED * dt);
    // Multiply with projection matrix
    mat4.multiply(transformMatrix, pvMatrix, rotateMatrix);

    // Apply transformations
    positions = transformPoints(positions, transformMatrix);

    model.positions2d = positions;
  }

  function drawModel() {
    drawPoints(ctx, model.positions2d, center);
    drawPolygons(ctx, model.cells, model.positions2d, center);
  }

  function draw() {
    ctx.clearRect(0, 0, width, height);
    drawModel();
  }

  function onTick(dt) {
    width = ctx.canvas.width;
    height = ctx.canvas.height;

    center = [width / 4, height / 2];

    update(dt);
    draw();
  }

  function init() {
    pvMatrix = getPVMatrix();
    model = generateModel();
  }

  init();

  return {
    onTick: onTick
  };
})();
