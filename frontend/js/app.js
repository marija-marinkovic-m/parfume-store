require('./inputs');
require('./textarea');
require('./bg-video');
require('./navigation');
require('./page-transitions');
require('./intro');
require('./bg-music');
require('./sounds');
require('./tabs');
require('./product-nav');
require('./product-page');
require('./lang-dropdown');
require('./shop');

// Start canvas animation
var changeAnimation = require('./canvas-animations').changeAnimation;

var barbaContainer = document.querySelector('.barba-container');
var animation = barbaContainer.dataset.canvasAnim;

changeAnimation(animation);
