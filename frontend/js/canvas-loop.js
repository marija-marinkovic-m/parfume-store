var canvasLoop = require('canvas-loop');

module.exports = (function() {
  function CreateCanvasLoop(canvas) {
    return  canvasLoop(canvas);
  }

  return CreateCanvasLoop;
})();
