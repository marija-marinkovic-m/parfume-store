var helpers = {
  // Ajax fetch url
  fetch: function(url, onload, onprogress) {
    var xhr = new XMLHttpRequest();

    xhr.onprogress = onprogress;

    xhr.onload = function() {
      onload(this);
      xhr.onload = null;
      xhr.onprogress = null;
    };

    // Retry load
    xhr.onerror = function() {
      setTimeout(function() {
        fetch(url, onload);
      }, 500);
    };

    // Send request
    xhr.open('GET', url);
    xhr.send();
  },

  attachScript: function(src) {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    document.head.appendChild(script);
    script.async = false;
    script.src = src;
  },

  cacheSound: function(src) {
    var audio = new Audio(src);
    document.body.appendChild(audio);
  },

  $: function(selector, parent) {
    if (!selector || selector === '#') return null;
    var node = parent || document;
    return node.querySelector(selector);
  },

  $$: function(selector, parent) {
    var node = parent || document;
    var elemCollection = node.querySelectorAll(selector);
    return Array.prototype.slice.apply(elemCollection);
  },

  getAnimationStartEvent: (function() {
    var animationStartEvent = '';

    function animationstart() {
      if (animationStartEvent) return animationStartEvent;

      var events = {
        'animation': 'animationstart',
        'OAnimation': 'oAnimationStart',
        'MozAnimation': 'animationstart',
        'WebkitAnimation': 'webkitAnimationStart'
      };

      var el = document.createElement('fakeelement');

      for (var t in events) {
        if (el.style[t] !== undefined) {
          animationStartEvent = events[t];
          return animationStartEvent;
        }
      }
    }

    return animationstart;
  })(),

  getAnimationEndEvent: (function() {
    var animationEndEvent = '';

    function animationend() {
      if (animationEndEvent) return animationEndEvent;

      var events = {
        'animation': 'animationend',
        'OAnimation': 'oAnimationEnd',
        'MozAnimation': 'animationend',
        'WebkitAnimation': 'webkitAnimationEnd'
      };

      var el = document.createElement('fakeelement');

      for (var t in events) {
        if (el.style[t] !== undefined) {
          animationEndEvent = events[t];
          return animationEndEvent;
        }
      }
    }

    return animationend;
  })(),

  getTransitionEndEvent: (function() {
    var transitionEndEvent = '';

    function transitionend() {
      if (transitionEndEvent) return transitionEndEvent;

      var events = {
        'transition': 'transitionend',
        'OTransition': 'oTransitionEnd',
        'MozTransition': 'transitionend',
        'WebkitTransition': 'webkitTransitionEnd'
      };

      var el = document.createElement('fakeelement');

      for (var t in events) {
        if (el.style[t] !== undefined) {
          transitionEndEvent = events[t];
          return transitionEndEvent;
        }
      }
    }

    return transitionend;
  })(),
  sign: function(number) {
    if (!number) return 0;
    return number < 0 ? -1 : 1;
  },

  hypot: function(x1, y1, x2, y2) {
    var dx = x1 - x2;
    var dy = y1 - y2;
    var hypot = Math.sqrt(dx * dx + dy * dy);
    return hypot;
  }
};

module.exports = helpers;
