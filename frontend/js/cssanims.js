var Promise = require('bluebird');
var trigger = require('dispatch-event');
var $$ = require('./helpers').$$;
var animationEnd = require('./helpers').getAnimationEndEvent();
var classNames = require('./class-names');
var ANIM_CLASS = 'js-animated';

module.exports = (function() {
  function clearAnimation(el) {
    var animation = el.dataset && el.dataset.anim;
    var animIn = el.dataset && el.dataset.animIn;
    var animOut = el.dataset && el.dataset.animOut;
    var once = el.dataset && el.dataset.animOnce;

    if (!once) {
      el.classList.remove(classNames.animated);
      el.classList.remove( animation);
    }
    el.classList.remove(classNames.animating);

    if (animIn) el.classList.remove(animIn);
    if (animOut) el.classList.remove(animOut);

    return Promise.resolve();
  }

  function clearAnimationOnElements(elements) {
    elements.forEach(function(el) {
      setTimeout(function() {
        clearAnimation(el);
      });
    });
  }

  function clearAnimationEl() {
    this.removeEventListener(animationEnd, clearAnimationEl);
    return Promise.resolve(clearAnimation(this));
  }

  function animateElement(el) {
    var animation = el.dataset && el.dataset.anim;
    if (!animation) return Promise.reject();

    return new Promise(function(resolve) {
      var delay = parseInt(el.dataset.animDelay, 10);

      setTimeout(function() {
        // Clear previous animation
        trigger(el, animationEnd);
        el.classList.remove(classNames.animated);
        el.classList.remove(animation);
        el.classList.remove(classNames.hidden);
        el.classList.remove(classNames.transparent);

        // Animate element
        el.classList.add(classNames.animated);
        el.classList.add(classNames.animating);
        el.classList.add(animation);
        el.addEventListener(animationEnd, clearAnimationEl);

        resolve();
      }, delay);
    });
  }

  function animateElements(parent) {
    var node = parent || document;
    var elements = $$('.' + ANIM_CLASS, node);

    if (elements.length === -1) return Promise.reject();

    return Promise.all(
      $$('.' + ANIM_CLASS, node).map(function(el) {
        return animateElement(el);
      })
    );
  }

  function animateIn(el) {
    var animation = el && el.dataset && el.dataset.animIn;

    if (!animation) return Promise.reject();

    function anim() {
      // add / remove classes
      return new Promise(function(resolve) {
        var delay = parseInt(el.dataset.animDelay, 10);

        setTimeout(function() {
          // Clear previous animation
          trigger(el, animationEnd);
          el.classList.remove(classNames.hidden);
          el.classList.remove(classNames.transparent);
          el.classList.add(classNames.animated);
          el.classList.add(classNames.animating);
          el.classList.add(animation);

          // add event to remove classes
          el.addEventListener(animationEnd, function clearAnimationIn() {
            this.removeEventListener(animationEnd, clearAnimationIn);
            resolve(clearAnimation(this));
          });
        }, delay);
      });
    }

    return clearAnimation(el).then(anim);
  }

  function animateOut(el) {
    var animation = el && el.dataset && el.dataset.animOut;

    if (!animation) return Promise.reject();

    function anim() {
      return new Promise(function(resolve) {
        // add / remove classes
        setTimeout(function() {
          // Clear previous animation
          trigger(el, animationEnd);
          el.classList.add(classNames.animated);
          el.classList.add(classNames.animating);
          el.classList.add(animation);

          // add event to remove classes
          el.addEventListener(animationEnd, function clearAnimationOut() {
            this.removeEventListener(animationEnd, clearAnimationOut);
            this.classList.add(classNames.hidden);
            resolve(clearAnimation(this));
          });
        });
      });
    }

    return clearAnimation(el).then(anim);
  }

  return {
    animateElement: animateElement,
    animateElements: animateElements,
    animateIn: animateIn,
    animateOut: animateOut,
    clearAnimationOnElements: clearAnimationOnElements
  };
})();
