var trigger = require('dispatch-event');
var $ = require('./helpers').$;
var $$ = require('./helpers').$$;
var carousel = require('./carousel');
var IngredientsAnim = require('./ingredients-animation');
var ingredientsAnim = {};

function changeActiveNavItem(href) {
  if (!href) return;

  // Clear current active item
  $$('.js-nav-items .is-active').forEach(function(el) {
    el.classList.remove('is-active');
  });

  // Set active item
  $$('.js-nav-items [href*="' + href + '"]').forEach(function(el) {
    el.classList.add('is-active');
  });
}

module.exports = {
  namespace: 'leclair',
  onEnter: function() {
    carousel.init(this.container);
    var href = window.location.pathname.slice(1);
    changeActiveNavItem(href);

    var ingredientsCanvas = $('.js-ingredients-canvas');
    if (ingredientsCanvas) {
      ingredientsAnim = new IngredientsAnim(ingredientsCanvas);
    }
  },
  onEnterCompleted: function() {
    trigger(window, 'resize'); // Force layout reflow
  },
  onLeave: function() {
    carousel.destroy(this.container);
    if (ingredientsAnim.destroy) {
      ingredientsAnim.destroy();
      ingredientsAnim = {};
    }
  }
};
