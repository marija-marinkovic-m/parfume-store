var delegate = require('dom-delegate');
var classNames = require('./class-names');
var $$ = require('./helpers').$$;

var LANG_DROPDOWN = '.js-lang-dropdown';
var LANG_DROPDOWN_LIST = '.js-lang-dropdown__list';

var eventDelegate = delegate(document.body);

var dropdownOpened = false;
var autoCloseTimeout = null;

function openDropdown(e) {
  e.preventDefault();
  $$(LANG_DROPDOWN + ',' + LANG_DROPDOWN_LIST).forEach(function(dropdown) {
    dropdown.classList.add(classNames.active);
  });
  dropdownOpened = true;
}

function closeDropdown() {
  $$(LANG_DROPDOWN + ',' + LANG_DROPDOWN_LIST).forEach(function(dropdown) {
    dropdown.classList.remove(classNames.active);
  });
  dropdownOpened = false;
}

function toggleDropdown(e) {
  dropdownOpened ? closeDropdown(e) : openDropdown(e);
}

eventDelegate.on('click', LANG_DROPDOWN, toggleDropdown);
eventDelegate.on(
  'mouseover', LANG_DROPDOWN + ',' + LANG_DROPDOWN_LIST, function() {
    clearTimeout(autoCloseTimeout);
  }
);

eventDelegate.on(
  'mouseout', LANG_DROPDOWN + ',' + LANG_DROPDOWN_LIST, function() {
    autoCloseTimeout = setTimeout(function() {
      if (dropdownOpened) {
        closeDropdown();
      }
    }, 800);
  }
);
