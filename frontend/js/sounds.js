var Howl = require('howler').Howl;
var delegate = require('dom-delegate');
var animationStart = require('./helpers').getAnimationStartEvent();
var eventDelegate = delegate(document.body);

var click = new Howl({
  src: ['/sound/click.mp3']
});

var bloop = new Howl({
  src: ['/sound/bloop.mp3']
});

var bloops = new Howl({
  src: ['/sound/bloops.mp3']
});

var pop = new Howl({
  src: ['/sound/pop.mp3']
});

// Add event handlers
eventDelegate.on('click', 'a, button', function() {
  click.play();
});

eventDelegate.on('mouseenter', '.js-bloop-hover', function() {
  if (bloop.playing()) bloop.stop();
  bloop.play();
}, true);

eventDelegate.on('click', '.js-bloop-click', function() {
  if (bloop.playing()) bloop.stop();
  bloop.play();
});

eventDelegate.on(animationStart, '.js-bloops', function() {
  bloops.play();
});

eventDelegate.on('mouseenter', '.js-drop', function() {
  if (pop.playing()) pop.stop();
  pop.play();
}, true);

eventDelegate.on('click', '.js-drop', function() {
  if (bloop.playing()) bloop.stop();
  bloop.play();
});
