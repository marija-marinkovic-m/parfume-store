require('svgxuse');
var $ = require('./helpers').$;
var loader = require('./loader');
var animateElements = require('./cssanims').animateElements;

function loadScripts() {
  if (loader.isInjected()) {
    loader.loadAssets([
      '/js/vendor.js',
      '/js/app.js'
    ]);
  } else {
    setTimeout(loadScripts, 50);
  }
}
window.addEventListener('load', function() {
  loader.injectLoader('.js-loader__anim');
  animateElements($('.js-loader'));
  loadScripts();
  loader.transitionToPage('.js-loader', '.js-page');
});
