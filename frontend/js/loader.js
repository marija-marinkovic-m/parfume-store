var TWEEN = require('tween.js');
var Howl = require('howler').Howl;
var trigger = require('dispatch-event');
var fetch = require('./helpers').fetch;
var attachScript = require('./helpers').attachScript;
var cacheSound = require('./helpers').cacheSound;
var $ = require('./helpers').$;
var animateElement = require('./cssanims').animateElement;
var animateIn = require('./cssanims').animateIn;
var animateOut = require('./cssanims').animateOut;
var animateElements = require('./cssanims').animateElements;
var animationEnd = require('./helpers').getAnimationEndEvent();

module.exports = (function() {
  var LOADER_IMAGE = '/images/loader.svg';
  var injected = false;
  var assetsNumber = 1;
  var assetsLoaded = 0;
  var assetsQueue = [];
  var progress = 0;
  var animating = true;

  // Sound config
  var airSound = new Howl({
    src: ['/sound/air.mp3']
  });

  // Animation parameters
  var anim = {
    x: {
      from: -47,
      to: -2010
    },
    y: {
      from: 285,
      to: 27
    },
    offset: {
      from: 45,
      to: 75
    },
    opacity: {
      from: 0,
      to: 1
    }
  };
  var liquidProps = {
    x: anim.x.from,
    y: anim.y.from
  };
  var splatProps = {
    offset: anim.offset.from,
    opacity: anim.opacity.from
  };

  // Animaton elements
  var liquidElement;
  var splatElement;

  // Animation Tweens
  var liquidTween;
  var splatTween;

  function initTweens() {
    liquidProps = {
      x: anim.x.from,
      y: anim.y.from
    };
    splatProps = {
      offset: anim.offset.from,
      opacity: anim.opacity.from
    };

    liquidTween = new TWEEN.Tween(liquidProps)
      .onUpdate(function() {
        liquidElement.setAttribute('x', this.x);
        liquidElement.setAttribute('y', this.y);
      });

    splatTween = new TWEEN.Tween(splatProps)
      .onUpdate(function() {
        splatElement.style.strokeDashoffset = this.offset;
        splatElement.style.opacity = this.opacity;
      })
      .onComplete(function() {
        // Stop animation loop
        animating = false;
      });
  }

  function resetAnimation() {
    // Reset style & attributes
    liquidElement.setAttribute('x', anim.x.from);
    liquidElement.setAttribute('y', anim.y.from);
    splatElement.style.strokeDashoffset = anim.offset.from;
    splatElement.style.opacity = anim.opacity.from;
    TWEEN.remove(liquidTween);
    TWEEN.remove(splatTween);
    initTweens();
  }

  function animateLiquid(animProgress, duration) {
    var x = anim.x.from + (anim.x.to - anim.x.from) * animProgress;
    var y = anim.y.from + (anim.y.to - anim.y.from) * animProgress;

    return liquidTween
      .to({
        x: x,
        y: y
      }, duration || 100)
      .start()
      .easing(TWEEN.Easing.Circular.Out);
  }

  function animateSplat() {
    splatTween.to({
      offset: anim.offset.to,
      opacity: anim.opacity.to
    }, 1000)
      .easing(TWEEN.Easing.Quintic.Out)
      .start();
  }

  // Animation loop
  function animationLoop() {
    // Escape loop if finished animation
    if (!animating) return false;

    // Update tweens
    TWEEN.update();

    // Restart loop
    requestAnimationFrame(animationLoop);
  }

  function updateProgress(value) {
    progress += value / assetsNumber;

    // Fill bottle up to 75%
    animateLiquid(progress * 0.75);
  }

  function loadComplete() {
    if (assetsNumber !== assetsLoaded) return;

    assetsQueue.forEach(function(cb) { cb(); });

    animateLiquid(0.75, 100).onComplete(function() {
      animateLiquid(1, 1700).onComplete(function() {
        airSound.play();
        animateSplat();
      });
    });
  }

  function loadAsset(asset, idx) {
    // Fetch with update progress and on load
    var localProgress = 0;

    function onprogress(ev) {
      if (ev.lengthComputable) {
        var newProgress = (ev.loaded / ev.total);
        updateProgress(newProgress - localProgress);
        localProgress = newProgress;
      }
    }

    function onload() {
      assetsLoaded++;
      updateProgress(1 - localProgress);

      // Todo: attach other asset types
      if (/\.js$/.test(asset)) {
        assetsQueue[idx] = attachScript.bind(this, asset);
      }

      if (/\.mp3$/.test(asset)) {
        assetsQueue[idx] = cacheSound.bind(this, asset);
      }

      loadComplete();
    }

    fetch(asset, onload, onprogress);
  }

  function loadAssets(assets) {
    if (!assets.length) return;
    progress = 0;
    assetsLoaded = 0;
    assetsNumber = assets.length;
    assetsQueue = [];
    animating = true;
    animationLoop();
    resetAnimation();
    assets.forEach(loadAsset);
  }

  function injectLoader(selector) {
    var svgElement;

    function injectSvg(svg) {
      var el = document.querySelector(selector);

      // Fallback to body tag
      if (!el) el = document.body;

      el.insertBefore(svg, el.firstChild);
      injected = true;
    }

    function onload(response) {
      svgElement = response.responseXML.documentElement;
      liquidElement = svgElement.querySelector('.js-loader__liquid');
      splatElement = svgElement.querySelector('.js-loader__splat');
      injectSvg(svgElement);
    }

    if (!injected) fetch(LOADER_IMAGE, onload);
  }

  function isInjected() {
    return injected;
  }

  function transitionToPage(loaderSelector, pageSelector) {
    var loader = $(loaderSelector);
    var page = $(pageSelector);

    if (!loader || !page) return null;

    function transitionPage() {
      animateElement(page);
      animateElements().then(function() {
        // Emit resize event to properly initialize dynamic components
        // (carousel & similar)
        trigger(window, 'resize');
      });

      // Show header/footer
      var header = document.querySelector('.js-header');
      var footer = document.querySelector('.js-footer');
      var barbaContainer = document.querySelector('.barba-container');

      if (!barbaContainer.dataset.noHeader.length > 0) {
        animateIn(header);
        animateIn(footer);
      }

      // Remove event listener
      loader.removeEventListener(animationEnd, transitionPage);
    }

    function removeLoader() {
      if (animating) return setTimeout(removeLoader, 15);
      animateOut(loader);
      loader.addEventListener(animationEnd, transitionPage);
    }

    removeLoader();
  }

  return {
    injectLoader: injectLoader,
    loadAssets: loadAssets,
    isInjected: isInjected,
    transitionToPage: transitionToPage
  };
})();
