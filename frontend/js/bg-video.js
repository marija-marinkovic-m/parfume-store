var BackgroundVideo = require('background-video');

var VIDEO_URL = '/video/bg.mp4';

module.exports = new BackgroundVideo('.js-bg-video', {
  src: [ VIDEO_URL ],
  parallax: {
    effect: 0
  },
  onReady: function() {
    // Use onReady() to prevent flickers or force loading state
    var vidParent = document.querySelector('.' + this.bvVideoWrapClass);

    // Remove 'hidden' attribute from video element
    vidParent.lastChild.removeAttribute('hidden');
  }
});

