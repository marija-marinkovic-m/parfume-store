/* global gulp, config */
'use strict';

var argv = require('yargs').argv;
var run = require('gulp-run');
var openBrowser = require('open');
var projectName = (argv.name || require('../../package.json').name)
  .replace(/\s/g, '-')
  .toLowerCase();

var DEPLOY_FOLDER = config.buildPath + '*';
var SSH_HOST = 'ubuntu@preview.saturized.com';
var DOMAIN = 'preview.saturized.com';
var FOLDER = '/var/www/preview/' + projectName;

function deleteFolderCommand() {
  return 'ssh ' + SSH_HOST + ' "rm -rf ' + FOLDER + ' && mkdir ' + FOLDER + '"';
}

function deployFolderCommand() {
  return 'scp -r ' + DEPLOY_FOLDER + ' ' + SSH_HOST + ':' + FOLDER;
}

gulp.task('deploy', ['build'], function() {
  return run(deleteFolderCommand())
    .exec(function() {
      run(deployFolderCommand()).exec(function() {
        var fullUrl = 'http://' + projectName + '.' + DOMAIN;
        console.log('Deployed to ' + fullUrl + '. Opening browser...');
        openBrowser(fullUrl);
      });
    });
});
