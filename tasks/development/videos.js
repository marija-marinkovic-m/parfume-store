/* global gulp, config */
'use strict';

gulp.task('videos', function() {
  gulp.src(config.srcPath + config.videosDir + '*')
    .pipe(gulp.dest(config.buildPath + config.videosDir));
});
