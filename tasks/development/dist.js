/* global gulp, config, plumber */
'use strict';

var gulpIf     = require('gulp-if');
var rev        = require('gulp-rev');
var revReplace = require('gulp-rev-replace');
var revdel     = require('gulp-rev-delete-original');
var cleanCSS   = require('gulp-clean-css');
var uglify     = require('gulp-uglify');
var htmlmin    = require('gulp-htmlmin');
var lazypipe   = require('lazypipe');

var uglifyOptions = {
  mangle: true
};

var htmlminOptions = {
  collapseWhitespace: true,
  conservativeCollapse: true
};

var revAssets = lazypipe()
  .pipe(rev)
  .pipe(revdel);

gulp.task('dist:css', ['sass'], function() {
  return gulp.src(config.buildPath + '**/*.css')
    .pipe(plumber())
    .pipe(cleanCSS())
    .pipe(gulp.dest(config.buildPath));
});

gulp.task('dist:js', ['js'], function() {
  return gulp.src(config.buildPath + '**/*.js')
    .pipe(plumber())
    .pipe(uglify(uglifyOptions))
    .pipe(gulp.dest(config.buildPath));
});

gulp.task('dist:html', ['nunjucks'], function() {
  return gulp.src(config.buildPath + '*.html')
    .pipe(plumber())
    .pipe(htmlmin(htmlminOptions))
    .pipe(gulp.dest(config.buildPath));
});

gulp.task('dist', ['dist:css', 'dist:js', 'dist:html'], function() {
  return gulp.src([
    config.buildPath + '*.html',
    config.buildPath + '**/*.css',
    config.buildPath + '**/*.js'
  ])
    .pipe(gulpIf('!*.html', revAssets()))
    .pipe(revReplace())
    .pipe(gulp.dest(config.buildPath));
});
