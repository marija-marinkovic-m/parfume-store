/* global gulp, config, del */
'use strict';

gulp.task('clean:fonts', function(cb) {
  return del([
    config.buildPath + 'fonts/**'
  ], cb);
});

gulp.task('fonts', ['clean:fonts'], function() {
  return gulp.src(
    config.srcPath +
    config.fontsDir +
    '**/*.{ttf,woff,eof,eot,otf,svg}'
  ).pipe(gulp.dest(config.buildPath + config.fontsDir));
});
