/* global gulp, config */
'use strict';

var favicons = require('gulp-favicons');

gulp.task('favicons', function() {
  gulp.src(config.srcPath + config.faviconDir + 'favicon.png')
  .pipe(favicons({}))
  .pipe(gulp.dest(config.buildPath));
});
