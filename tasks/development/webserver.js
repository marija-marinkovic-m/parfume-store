/* global gulp, config */
'use strict';

var openBrowser = require('open');
var connect = require('gulp-connect');

gulp.task('webserver', ['default'], function() {
  connect.server({
    root: [config.buildPath, '.'],
    livereload: config.useLiveReload,
    fallback: config.buildPath + config.indexPage
  });

  openBrowser('http://localhost:8080');
});
