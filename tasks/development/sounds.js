/* global gulp, config */
'use strict';

gulp.task('sounds', function() {
  gulp.src(config.srcPath + config.soundsDir + '*')
    .pipe(gulp.dest(config.buildPath + config.soundsDir));
});
