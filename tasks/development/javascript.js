/* global gulp, config, del */
'use strict';

var eslint       = require('gulp-eslint');
var _            = require('lodash');
var browserify   = require('browserify');
var source       = require('vinyl-source-stream');
var bowerResolve = require('bower-resolve');
var nodeResolve  = require('resolve');
var connect      = require('gulp-connect');
var gutil        = require('gulp-util');
var fs           = require('fs');
var buffer       = require('vinyl-buffer');

var earlyDeps = [
  'tween.js',
  'bluebird',
  'howler',
  'svgxuse',
  'dispatch-event'
];

gulp.task('eslint', function() {
  return gulp.src(config.srcPath + config.jsDir + '**/*.js')
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});

gulp.task('clean:js', function(cb) {
  return del([
    config.buildPath + config.jsDir + '**/*.js'
  ], cb);
});

gulp.task('js-lint', ['eslint']);

/**
 * Source: https://github.com/sogko/gulp-recipes/
 */

/**
 * Helper function(s)
 */
function getBowerPackageIds() {
  // read bower.json and get dependencies' package ids
  var bowerManifest = {};
  try {
    bowerManifest = require('../../bower.json');
  } catch (e) {
    // does not have a bower.json manifest
  }
  return _.keys(bowerManifest.dependencies)
    .filter(function(el) { return earlyDeps.indexOf(el) === -1; }) || [];
}

function getNPMPackageIds() {
  // read package.json and get dependencies' package ids
  var packageManifest = {};
  try {
    packageManifest = require('../../package.json');
  } catch (e) {
    // does not have a package.json manifest
  }
  return _.keys(packageManifest.dependencies)
   .filter(function(el) { return earlyDeps.indexOf(el) === -1; }) || [];
}

function browserifyBundle(b, sourceFile) {
  var stream = b.bundle()
    .on('error', function(err) {
      gutil.log(err.message);
      // end this stream
      this.emit('end');
    })
    .pipe(source(sourceFile))
    // pipe additional tasks here (for eg: minifying / uglifying, etc)
    // remember to turn off name-mangling if needed when uglifying
    .pipe(connect.reload());

  stream.pipe(buffer()).pipe(gulp.dest(config.buildPath + config.jsDir));

  return stream;
}

function buildJS(sourceFile) {
  var b = browserify(config.srcPath + config.jsDir + sourceFile, {
    // Generate source maps
    debug: true
  });

  // mark vendor libraries defined in bower.json as an external library,
  // so that it does not get bundled with app.js.
  // instead, we will load vendor libraries from vendor.js bundle
  getBowerPackageIds().forEach(function(lib) {
    b.external(lib);
  });

  // do the similar thing, but for npm-managed modules.
  // resolve path using 'resolve' module
  getNPMPackageIds().forEach(function(id) {
    b.external(id);
  });

  return browserifyBundle(b, sourceFile);
}

gulp.task(
  'js',
  ['clean:js', 'js:build-vendor', 'js:build-app', 'js:build-early']
);

gulp.task('js:build-vendor', function() {
  // this task will go through ./bower.json and
  // uses bower-resolve to resolve its full path.
  // the full path will then be added to the bundle using require()

  var b = browserify({
    // Generate source maps
    debug: true
  });

  // get all bower components ids and use 'bower-resolve' to resolve
  // the ids to their full path, which we need for require()
  getBowerPackageIds().forEach(function(id) {
    var resolvedPath = '';
    try {
      resolvedPath = bowerResolve.fastReadSync(id);
    } catch (e) {
      // package does not have bower.json
      return null;
    }

    // If resolvedPath is not js nor file exists
    if (!/\.js$/.test(resolvedPath) ||
      !fs.existsSync(resolvedPath)) return null;

    return b.require(resolvedPath, {
      // exposes the package id, so that we can require() from our code.
      expose: id
    });
  });

  // do the similar thing, but for npm-managed modules.
  // resolve path using 'resolve' module
  getNPMPackageIds().forEach(function(id) {
    var resolvedPath = '';
    try {
      resolvedPath = nodeResolve.sync(id);
    } catch (e) {
      // package does not have .js file.
      return null;
    }

    b.require(resolvedPath, { expose: id });
  });

  return browserifyBundle(b, 'vendor.js');
});

gulp.task('js:build-app', function() {
  return buildJS('app.js');
});

gulp.task('js:build-early', function() {
  return buildJS('early.js');
});
