/* global gulp, config, del, plumber */
'use strict';

var imagemin = require('gulp-imagemin');
var pngcrush = require('imagemin-pngcrush');

gulp.task('clean:images', function(cb) {
  return del([
    config.buildPath + config.imgDir + '**'
  ], cb);
});

gulp.task('images', ['clean:images'], function() {
  return gulp.src(config.srcPath + config.imgDir + '**/*')
    .pipe(plumber())
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngcrush()]
    }))
    .pipe(gulp.dest(config.buildPath + config.imgDir));
});
