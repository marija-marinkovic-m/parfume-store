/* global gulp, config, del, plumber */
'use strict';

var rename = require('gulp-rename');
var svgmin = require('gulp-svgmin');
var svgSymbols = require('gulp-svg-symbols');

gulp.task('clean:icons', function(cb) {
  return del([
    config.buildPath + config.iconsDir + '**'
  ], cb);
});

gulp.task('icons', ['clean:icons'], function() {
  return gulp.src(config.srcPath + config.iconsDir + '**/*.svg')
    .pipe(plumber())
    .pipe(svgmin())
    .pipe(svgSymbols({
      templates: ['default-svg']
    }))
    .pipe(rename('icons.svg'))
    .pipe(gulp.dest(config.buildPath + config.iconsDir));
});
